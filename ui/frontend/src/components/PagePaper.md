## Examples

```jsx
<PagePaper>
    A default page.
</PagePaper>
```

Unknown props are broadcast to the Material-UI ``Paper`` element:

```jsx
<PagePaper square={true} elevation={6}>
    A square, higher up page.
</PagePaper>
```
