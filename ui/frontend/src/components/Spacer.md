## Examples

```jsx
<div>
    <h2>No spacing</h2>
    <div>line 1</div>
    <div>line 2</div>

    <h2>With spacing</h2>
    <div>line 1</div>
    <Spacer />
    <div>line 2</div>
</div>
```
