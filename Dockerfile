# Use node container to build frontend app
FROM node:10 as frontend-builder

# Do everything relative to /usr/src/app which is where we install our
# application.
WORKDIR /usr/src/app

# Install packages and build frontend
ADD ./ui/frontend/ ./
RUN npm install && npm run build

# Build webapp image
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/django:2.2-py3.7

# From now on, work in the application directory
WORKDIR /usr/src/app

# Copy Docker configuration and install any requirements. We install
# requirements/docker.txt last to allow it to override any versions in
# requirements/requirements.txt.
ADD ./requirements/* ./requirements/
RUN \
	apk add --no-cache build-base \
		postgresql-dev libffi-dev libxml2-dev libxslt-dev \
	&& pip install --no-cache-dir -r requirements/base.txt \
	&& pip install --no-cache-dir -r requirements/docker.txt \
	&& apk del g++ gcc binutils

# Copy the remaining files over
ADD . .
COPY --from=frontend-builder /usr/src/app/build/ /usr/src/build/frontend/

# Default environment for image.  By default, we use the settings module bundled
# with this repo. Change DJANGO_SETTINGS_MODULE to install a custom settings.
#
# You probably want to modify the following environment variables:
#
# DJANGO_DB_ENGINE, DJANGO_DB_HOST, DJANGO_DB_PORT, DJANGO_DB_USER
EXPOSE 8000
ENV \
	DJANGO_SETTINGS_MODULE=project.settings.docker \
	DJANGO_FRONTEND_APP_BUILD_DIR=/usr/src/build/frontend/ \
	PORT=8000

# Collect static files. We provide placeholder values for required settings.
RUN \
    EXTERNAL_SETTING_SECRET_KEY=placeholder \
    EXTERNAL_SETTING_DATABASES="{'default': {}}" \
    ./manage.py collectstatic

ENTRYPOINT ["./docker-entrypoint.sh"]
