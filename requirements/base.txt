# Requirements for Lecture Capture Preferences itself
django~=2.2
# Pin the version of psycopg to avoid https://github.com/psycopg/psycopg2/issues/1293
psycopg2-binary>=2.8,<2.9
# explicitly specify django-automationcommon's git repo since changes in
# automationcommon tend to be "ad hoc" and may need testing here without a
# corresponding pypi release. Recall that git branched may be explicitly given
# in the VCS URL.
git+https://gitlab.developers.cam.ac.uk/uis/devops/django/automationcommon@master#egg=django-automationcommon
git+https://gitlab.developers.cam.ac.uk/uis/devops/django/externalsettings@master
django-ucamwebauth>=1.4.5

# Make sure we use a version of django-ucamlookup with the latest lookup
# certificate.
django-ucamlookup>=3.0.5,<4

# API-related Django packages
djangorestframework
django-cors-headers
drf-yasg
django-crispy-forms
django-filter

# We need at least version 4 of whitenoise to make use of the index_file
# configuration option.
whitenoise>=4.0

# Required for loading fixtures from YAML files
PyYAML

# For an improved manage.py shell experience
ipython

# So that tests may be run within the container
tox

# Serving
gunicorn
