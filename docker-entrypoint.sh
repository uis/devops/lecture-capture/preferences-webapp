#!/usr/bin/env sh

# Run any database migrations
./manage.py migrate

# Start the server
exec gunicorn \
  --name preferences \
  --bind :$PORT \
  --worker-class gthread \
  --workers 4 \
  --threads 4 \
  --log-level=info \
  --log-file=- \
  --access-logfile=- \
  --capture-output \
  project.wsgi
